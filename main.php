#!/usr/bin/php -q
<?php
/* Make sure you enable 'ftp' in 'php.ini'! */

/* Includes */

/* Setup Args */
$argc = $_SERVER["argc"]; //Argument count
$argv = $_SERVER["argv"]; //Array of arguments


/* Initialise Flags */
$debug = false;
$move = false;
$do = false;

/* Set flags */
if ($argc > 1)
{
	foreach ($argv as $arg)
	{
		switch ($arg)
		{
			case "--DEBUG":
				$debug = true;
				break;
			case "-m":
				$move = true;
				break;
			case "-d":
				$do = true;
				break;
		}
	}
}

/* Get INI file */
$ftpLogin = array('host'=>'','port'=>'','ftps'=>false,'username'=>'','password'=>''); //Array with login data.
if ($iniFile = parse_ini_file('login.ini') or die("Could not open INI file."))
{
	/* Parse data from INI file to ftpLogin array */
	$ftpLogin['host'] = $iniFile['host'];
	$ftpLogin['port'] = $iniFile['port'];
	$ftpLogin['ftps'] = $iniFile['ftps'];

	$ftpLogin['username'] = $iniFile['username'];
	$ftpLogin['password'] = $iniFile['password'];
	
	$iniFileBackup = $iniFile;
	
	
}


	/* Check if ftps is true or false */
if ($ftpLogin['ftps'] == '1' || $ftpLogin['ftps'] == 1 || $ftpLogin['ftps'] == true)
{
	$ftpLogin['ftps'] = true;
}
else
{
	$ftpLogin['ftps'] = false;
}

/* Setup connection */


	/* Connect and Login */
if ($ftpLogin['ftps'] !== true)
{
	/* Connect without SSL */
	$conn_id = ftp_connect($ftpLogin['host'], $ftpLogin['port'], 15) or die('Could not connect to server!');
}
else
{
	/* Connect with SSL */
	$conn_id = ftp_ssl_connect($ftpLogin['host'], $ftpLogin['port'], 15) or die ('Could not connect to to ssl server!');
}

	/* Login */
$login_result = ftp_login($conn_id, $ftpLogin['username'], $ftpLogin['password']);



/* Execute Flag Functions */
if ($debug == true)
{
	
	print_r(get_defined_vars());
	var_dump($iniFile);
	var_dump($ftpLogin);
}
if ($move == true)
{
	personal_msbMoveReadyToNas("/files/ready");
}

if ($do == true)
{
	program_start($iniFile['InternalDownloadFolder'], $iniFile['ExternalDownloadFolder']);
}

/* Functions */

function program_start($localPath, $serverPath, $redo = false)
{
	global $iniFile;
	global $iniFileBackup;
	if ($redo == true)
	{
		$iniFile['InternalDownloadFolder'] = $localPath;
		$iniFile['ExternalDownloadFolder'] = $serverPath;
	}
	else
	{
		$iniFile = $iniFileBackup;
	}
	$internalFiles = getInternalFilesToMemory($localPath);
	$externalFiles = getExternalFilesToMemory($serverPath);
	$externalFilesWithPath = getExternalFilesToMemory($serverPath, true);
	$compared = compare($internalFiles, $externalFiles);
	dl_not_same($compared, $internalFiles, $externalFiles, $externalFilesWithPath);
}

function ftp_isDir($host, $port, $username, $password, $ssl, $folder)
{
	global $debug;
	
	if ($ssl == true)
	{
		$ftp = "ftps://";
	}
	else
	{
		$ftp = "ftp://";
	}
	
	$path = $ftp . $username . ":" . $password . "@" . $host . ":" . $port . $folder;
	
	if ($debug == true)
	{
		echo $path . "\n";
	}
	
	return is_dir($path);
}


function getExternalFilesToMemory($path = "/", $returnPath = false)
{
	global $conn_id;
	global $debug;

	/* List raw files in FTP server */
	$files = ftp_nlist($conn_id, $path);

	if ($debug == true)
	{
		echo "getExternalFilesToMemory(): files:";
		var_dump($files);
	}

	$fileNames = array();
	foreach ($files as $file)
	{
		/* Remove path */
		$fileNames[] = basename($file);
	}
	
	if ($debug == true)
	{
		echo "GetExternalFilesToMemory: fileNames:";
		var_dump($fileNames);
	}

	if ($returnPath == false)
	{
		return $fileNames;
	}
	else
	{
		return $files;
	}	
}

function getInternalFilesToMemory($path = "/")
{
	global $debug;
	$files = scandir($path);


	if ($debug == true)
	{
		echo "getInternalFilesToMemory(): Files:";
		var_dump($files);
	}

	return $files;
}

function compare($array1 = array(), $array2 = array())
{
	global $debug;
	$same = array();
	if ($array1 !== null && $array2 !== null && is_array($array1) && is_array($array2))
	{
		foreach($array1 as $arr1)
		{
			foreach ($array2 as $arr2)
			{
				if ($arr1 == $arr2)
				{
					$same[] = $arr1;
				}
			}
		}
		return $same;
	}
	else
	{
		if ($debug == true)
		{
			echo "Input error in compare()!";
		}
	}

}

function dl_not_same($compareArray, $internalFiles, $externalFiles, $externalFilesWithPath)
{
	global $debug;
	global $iniFile;
	global $iniFileBackup;
	global $ftpLogin;
	global $conn_id;
	
	if ($debug == true)
	{
		echo "dl_not_same(): compare:";
		var_dump($compareArray);
		echo "dl_not_same(): internal:";
		var_dump($internalFiles);
		echo "dl_not_same(): external:";
		var_dump($externalFiles);
	}
	foreach ($externalFilesWithPath as $ext)
	{
		if (!in_array(basename($ext), $compareArray))
		{
			$downloadFolder = $iniFile['InternalDownloadFolder'] . basename($ext);
			if (ftp_isDir($ftpLogin['host'], $ftpLogin['port'], $ftpLogin['username'], $ftpLogin['password'], $ftpLogin['ftps'], $ext))
			{
				$newExternalFolder = $iniFile['ExternalDownloadFolder'] . basename($ext) . "/";
				$newInternalFolder = $iniFile['InternalDownloadFolder'] . basename($ext) . "/";
				echo basename($ext) . " is a folder!\nScanning for " . basename($ext) . ".\n";
				if (!is_dir($iniFile['InternalDownloadFolder'] . basename($ext)))
				{
					echo basename($ext) . " does not exist!\nCreating " . basename($ext) . "...\n";
					mkdir($iniFile['InternalDownloadFolder'] . basename($ext));
				}
				program_start($newInternalFolder, $newExternalFolder, true);
			}
			else
			{
				echo "Downloading " . basename($ext) . "...";
				if (ftp_get($conn_id, $downloadFolder, $ext, FTP_ASCII))
				{
					echo "Downloaded: " . basename($ext) . ".\n";
				}
				
			}
			
			
		}
		
		$iniFile = $iniFileBackup;
	
	}
}


?>
